/// Collection of helper functions validating string formats.
///
/// The functions are made to be used with the [`Authenticate`] trait.
///
/// [`Authenticate`]: ../../trait.Authenticate.html
pub mod string_validators;
/// Tools to extract `serde_json`::`Value` data
pub mod json_helper;